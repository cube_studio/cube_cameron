/*:
*
* @plugindesc This is a plugin for a whip mechanic.
*
* @author Ayo
*
* @help
* 
* no help at the moment
*
*/

(function() {
    // original function in rpg_objects.js
    Game_Player.prototype.moveByInput = function() {
        var temp = 0;
        this._x = Math.round(this._x);
        if (!this.isMoving() && this.canMove()) {
            var direction = this.getInputDirection();
            if (direction > 0) {
                $gameTemp.clearDestination();
            } 
            else if ($gameTemp.isDestinationValid()){
                var x = $gameTemp.destinationX();
                var y = $gameTemp.destinationY();
                direction = this.findDirectionTo(x, y);
            }
            if ((direction > 0) && (!TouchInput._mousePressed)) {
                this.executeMove(direction);
            }




        // changes start here...
            else if ((direction > 0) && (TouchInput._mousePressed)){
                // the left mouse key was pressed
                temp = 1;
            }
        }

        // changes start here...
        if (((temp == 1) && (TouchInput._pressedTime < 10)) || (Input._currentState['pageup'] && (Input._pressedTime < 10) && (Input._latestButton == 'pageup'))) {    // this.canPass(this.x, this.y, this.getInputDirection()) => function only allows whip movement if player is able to walk in direction that they are facing
            // The if block is only executed if all of the following is true:
            //      - currentState => the player pressed a button mapped to pageup ie Q or left clicks a mouse.
            //        Q should be used to reset the character incase they qet stuck.
            //      - pressedTime => the user must have recently pressed Q or clicked the mouse. 
            //        That is the first press spawns the whip, but holding it down does not continuously create the whip
            //      - lastestButton => prevents the whip from spawning in the case where Q is held down and then another key (ie arrows)
            //        is pressed. Without this check, the whip would spawn twice. Once when Q is first pressed and again when
            //        an arrow key presed.
            //
            // This block does not:
            //      - prevent a player from using whip in a "bad" direction. For example, using the whip while facing a 
            //        house would show the whip on top of the house. Reasonably, this should not occur in most
            //        situations. Players can also use the whip to jump onto tiles they shouldn't (potential sol'n exists).
            //      - The whip animation seems to have a high priority. So, if the character is behind a tree for example, the whip
            //        will appear on top of the tree. Reasonably, this should not occur. (no potential sol'n at the moment).
            //      - The whip cannot yet interact with objects and trigger events.


            // sets player's position to desired location
            if (temp == 1){

                // displacement in x and y direction of the mouse click
                // positive means to the right/down
                // negative means to the left/up
                var displacement_x = (Math.round(((Graphics.pageToCanvasX(TouchInput._x)) / 48) - 9));
                var displacement_y = (Math.round(((Graphics.pageToCanvasY(TouchInput._y)) / 48) - 7));

                // the potential new x and y location
                var poss_x = this.x + displacement_x;
                var poss_y = this.y + displacement_y;


                if (this.canPass(poss_x, poss_y, 2)){           
                    // checks to see if a character is allowed to go to that square
                    // if so, set their new position.
                    this._x = poss_x;
                    this._y = poss_y;

                    // change characters direction to correct position
                    if ((displacement_y <= 3) && (displacement_y >= -3) && (displacement_x > 0)){
                        // face right during jump and correct animation
                        this.setDirection(6);
                        this.requestAnimation(121);
                    }

                    else if ((displacement_y <= 3) && (displacement_y >= -3) && (displacement_x < 0)){
                        // face left during jump and correct animation
                        this.setDirection(4);
                        this.requestAnimation(123);
                    }

                    else if (displacement_y >= 0){
                        // face down during jump and correct animation
                        this.setDirection(2);
                        this.requestAnimation(124);
                    }

                    else {
                        // face up during jump and correct animation
                        this.setDirection(8);
                        this.requestAnimation(122);
                    }

                    // creates jump to place character in correct spot
                    var distance = 3;
                    this._jumpPeak =  10 + distance - this._moveSpeed;
                    this._jumpCount =  this._jumpPeak * 2;
                    this.resetStopCount();
                    this.straighten();
                }

                else{
                    $gameMessage.add('cannot go there');
                }
            }

            else {
                // Q was pressed

                var animDirection = this.direction(); // gets direction character is facing in so whip appears in correct direction

                if (animDirection == 6) {
                    // character is facing right
                    this.requestAnimation(121);
                }

                else if (animDirection == 8) {
                    // character is facing up
                    this.requestAnimation(122);
                }

                else if (animDirection == 4) {
                    // character is facing left
                    this.requestAnimation(123);
                }

                else if (animDirection == 2) {
                    // character is facing down
                    this.requestAnimation(124);
                }

                // set character's position to a place on the map with a tile they can walk on using Q.
                this._x = 19;
                this._y = 31;
                // creates jump to place character in correct spot
                var distance = 3;
                this._jumpPeak =  10 + distance - this._moveSpeed;
                this._jumpCount =  this._jumpPeak * 2;
                this.resetStopCount();
                this.straighten();
            }
        }
    };



    Game_Character.prototype.pull = function(eventId){
        if (Input.isTriggered("#q")){
            var eventX = $gameMap.event(eventId)._x;
            $gameMessage.add(eventX);
            var eventY = $gameMap.event(eventId)._y;
        
            var playerX = $gamePlayer.x;
            var playerY = $gamePlayer.y;
        
            var x_diff = Math.abs(playerX - eventX);
            var y_diff = Math.abs(playerY - eventY);
        
            if ( x_diff < distance){
                if (y_diff == 0){
                    return 
                }
            }else if (y_diff < distance && x_diff == 0){
                return true;
            }else{
                return false;
            }
        }
    
        
    }
})();